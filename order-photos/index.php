<?php
  session_start();
  define('PASSWORD', 'ELS2016');
  if (isset($_POST['submit'])) {
    if ($_POST['password'] !== PASSWORD) {
      $_SESSION['wrongPassword'] = true;
      $_SESSION['authorised'] = false;
      header('Location: ./verify/');
      exit;
    } else {
      $_SESSION['wrongPassword'] = false;
      $_SESSION['authorised'] = true;
    }
  } else {
    if (isset($_SESSION['authorised'])) {
      if ($_SESSION['authorised'] === false) {
        header('Location: ./verify/');
        exit;
      }
    } else {
      header('Location: ./verify/');
      exit;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="ELS School of Dance" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Stephanie Ridnell, Emma Scott">
  <link rel="apple-touch-icon" sizes="57x57" href="../img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="../img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="../img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="../img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="../img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="../img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="../img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
  <link rel="manifest" href="../img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title>ELS School of Dance</title>
  <link rel="stylesheet" href="../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../css/animate.css" />
  <link rel="stylesheet" href="../css/ionicons.min.css" />
  <link rel="stylesheet" href="../css/styles09022017.css" />
  <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" />
  <script src="../js/jquery-2.2.4.min.js"></script>
</head>
<body>
  <div class="bigger-form">
    <center>
      <h2 class="cursive text-blue">ELS School of Dance</h2>
      <h4>2016 End of Year Concert Photos</h4>
      <hr class="dark"/>
      <h3>Option 1</h3>
      <span class="best-value"><i class="ion-checkmark-circled"></i> Best Value</span>
      <h4>Order Digital Photos</h4>
      <p>For just $50 you can have a digital copy of every photo
        of your child sent to your email address.
      </p>
      <form name="digitalPhotos" class="form-horizontal" action="choose-sizes/finalise/" method="post">
        <input type="submit" name="digital" class="btn btn-blue" value="Click here to order digital photos" />
      </form>
    </center>
    <br/>
    <hr class="dark"/>
    <center>
      <h3>Option 2</h3>
      <h4>Order Prints</h4>
      <p>Alternatively, please choose which individual photos you'd like to order.</p>
      <p>Sizes available are 10 x 15cm (4x6") and 12.5 x 17.5cm (5x7").</p>
      <p>Prints are available in Glossy or Matte finishes.</p>
      <p>When you have finished selecting the photos you would like, click the next button.</p>
    </center>
      <?php
        $scanDIR = scandir('../img/concert_photos');
        $images = array();
        foreach($scanDIR as $key=>$value) {
          if (strlen($value) > 4) {
            $image = substr($value, 0, -4);
            $images[] = $image;
          }
        }
       ?>
       <p><span id="selectedNum">0</span> of <?php echo count($images) ?> selected</p>
       <form name="selectPhotoForm" class="form-horizontal" action="choose-sizes/" method="post">
         <input type="hidden" name="selected" id="selected" value="">
         <div class="row">
         <?php
           if (!isset($_SESSION['selectedPhotos'])) {
             $_SESSION['selectedPhotos'] = [];
           }
           foreach ($images as $index=>$image) {
             $class = 'order-photo-box';
             if (in_array($image, $_SESSION['selectedPhotos'])) {
               $class = 'order-photo-box order-photo-box-selected';
             }
             echo '
             <div class="col-sm-4 col-xs-6">
               <a id="'.$image.'" onclick="selectImage(\''.$image.'\')" class="'.$class.'">
               <img src="../img/concert_photos/'.$image.'.jpg" class="img-responsive" alt="'.$image.'" />
                 <div class="order-photo-box-caption">
                   <div class="order-photo-box-content">
                     <div>
                       <i class="icon-lg ion-checkmark-circled"></i>
                     </div>
                   </div>
                 </div>
               </a>
             </div>';
           }
         ?>
         </div>
         <div class="row">
           <div class="col-xs-6">
           </div>
           <div class="col-xs-6 text-right">
             <button onclick="setValue()" name="next" type="submit" class="btn btn-blue floaty-button">Next <i class="ion-arrow-right-c"></i></button>
           </div>
         </div>
       </form>
      <script>
        var session = eval('(<?php echo json_encode($_SESSION['selectedPhotos'])?>)');
        var selectedPhotos = [];
        if (session.length > 0) {
          selectedPhotos = session
          document.getElementById("selectedNum").innerHTML = session.length
        }
        var selectedNumber = 0;
        function selectImage(image) {
          if (document.getElementById(image).className.indexOf("order-photo-box-selected") === -1) {
            document.getElementById(image).className = "order-photo-box order-photo-box-selected";
            selectedPhotos.push(image);
          } else {
            document.getElementById(image).className = "order-photo-box";
            selectedPhotos.splice(selectedPhotos.indexOf(image), 1);
          }
          selectedNumber = selectedPhotos.length;
          document.getElementById("selectedNum").innerHTML = selectedNumber;
        }
        function setValue() {
          document.selectPhotoForm.selected.value = selectedPhotos;
        }
      </script>
    </div>
    <!--scripts loaded here for performance -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.easing.1.3.js"></script>
    <script src="../js/wow.js"></script>
    <script src="../js/scripts.js"></script>
    <script src="../js/moment.js"></script>
    <script src="../js/bootstrap-datetimepicker.min.js"></script>

</body>

</html>
