<?php
  session_start();

  if (isset($_SESSION['authorised'])) {
    if ($_SESSION['authorised'] === false) {
      header('Location: ../verify/');
      exit;
    }
  } else {
    header('Location: ../verify/');
    exit;
  }

  if(isset($_POST['next'])){
    if (strlen($_POST['selected']) > 0) {
      $selectedImages = explode(",", $_POST['selected']);
      $_SESSION['selectedPhotos'] = $selectedImages;
    } else {
      $selectedImages = false;
    }
  } else {
    $selectedImages = $_SESSION['selectedPhotos'];
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="ELS School of Dance" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Stephanie Ridnell, Emma Scott">
  <link rel="apple-touch-icon" sizes="57x57" href="../../img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="../../img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../../img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="../../img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../../img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="../../img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="../../img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="../../img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../../img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="../../img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="../../img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../img/favicon/favicon-16x16.png">
  <link rel="manifest" href="../../img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../../img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title>ELS School of Dance</title>
  <link rel="stylesheet" href="../../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../../css/animate.css" />
  <link rel="stylesheet" href="../../css/ionicons.min.css" />
  <link rel="stylesheet" href="../../css/styles09022017.css" />
  <link rel="stylesheet" href="../../css/bootstrap-datetimepicker.min.css" />
  <script src="../../js/jquery-2.2.4.min.js"></script>
</head>
<body>
  <div class="bigger-form">
    <center>
      <h2 class="cursive text-blue">ELS School of Dance</h2>
      <h4>Order Prints</h4>
    </center>
    <?php
      if ($selectedImages) {
        echo '<p class="small">Please review your selected photos and choose the sizes and photo finishes you would like.</p>
        <p class="small">Sizes available are 10 x 15cm (4x6") and 12.5 x 17.5cm (5x7").</p>
        <p class="small">Prints are available in Glossy or Matte finishes.</p>
      <form name="confirmPhotoForm" class="form-horizontal" action="finalise/" method="post">';
        foreach ($selectedImages as $index=>$image) {
          $class = 'order-photo-box';
          if ($index % 2 === 0) {
            echo '<div class="row">';
          }
          echo '
          <div class="col-sm-6">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="../../img/concert_photos/'.$image.'.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <br/>
                  <p class="order-form">
                    10 x 15cm (4x6") - $5.00 each<br/>
                    <input type="hidden" name="'.$image.'-4x6" id="'.$image.'-4x6" value="0"/>
                    <i onclick="changeValue(\'minus\', \''.$image.'-4x6\')" class="interactive-cursor ion-minus-circled"></i>
                    <span class="num" id="'.$image.'-4x6num">0</span>
                    <i onclick="changeValue(\'add\', \''.$image.'-4x6\')" class="interactive-cursor ion-plus-circled"></i> <br/>
                    12.5 x 17.5cm (5x7") - $7.00 each<br/>
                    <input type="hidden" name="'.$image.'-5x7" id="'.$image.'-5x7" value="0"/>
                    <i onclick="changeValue(\'minus\', \''.$image.'-5x7\')" class="interactive-cursor ion-minus-circled"></i>
                    <span class="num" id="'.$image.'-5x7num">0</span>
                    <i onclick="changeValue(\'add\', \''.$image.'-5x7\')" class="interactive-cursor ion-plus-circled"></i> <br/>
                    <input type="radio" name="'.$image.'-finish" value="glossy" checked> Glossy
                    <input type="radio" name="'.$image.'-finish" value="matte"> Matte
                  </p>
                </div>
                <div class="card-action">
                 Sub-Total: <span class="num" id="'.$image.'total">$0.00</span>
                </div>
              </div>
            </div>
          </div>';
          if ($index % 2 === 1) {
            echo '</div>';
          }
        }
        if (count($selectedImages) % 2 === 1) {
          echo '</div>';
        }
        echo '<div class="row">
          <div class="col-sm-12">
            <div class="card card-total">
              <div class="card-content">
                Total: <span class="num" id="total">$0.00</span>
              </div>
            </div>
          </div>
        </div>';
      } else {
        echo '<div class="row">
          <div class="col-sm-12">
            <div class="card card-total">
              <div class="card-content">
                You have not selected any images.
              </div>
            </div>
          </div>
        </div>';
      }
      ?>
      <div class="row">
        <div class="col-xs-6">
          <a href="../" class="btn btn-blue"><i class="ion-arrow-left-c"></i> Back</a>
        </div>
        <div class="col-xs-6 text-right">
          <?php
            if ($selectedImages) {
              echo '<button name="next" type="submit" class="btn btn-blue">Next <i class="ion-arrow-right-c"></i></button>';
            }
          ?>
        </div>
      </div>
    </form>
    <script>
      var total = 0;
      function changeValue(operator, imgSize) {
        var image = imgSize.substr(0, imgSize.length - 4);
        var size = imgSize.substr(imgSize.length - 3);
        var price = 5;
        if (size === '5x7') {
          price = 7
        }
        if (operator === 'add') {
          total += price
          document.getElementById(imgSize).value++;
          document.getElementById(imgSize + 'num').innerHTML = document.getElementById(imgSize).value;
        } else {
          if (document.getElementById(imgSize).value > 0) {
            total -= price
            document.getElementById(imgSize).value--;
            document.getElementById(imgSize + 'num').innerHTML = document.getElementById(imgSize).value;
          }
        }
        var totalPrice = document.getElementById(image + '-4x6').value * 5 + document.getElementById(image + '-5x7').value * 7
        document.getElementById(image + 'total').innerHTML = '$' + totalPrice + '.00';
        document.getElementById('total').innerHTML = '$' + total + '.00';
      }
    </script>
  </div>
    <!--scripts loaded here for performance -->
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.easing.1.3.js"></script>
    <script src="../../js/wow.js"></script>
    <script src="../../js/scripts.js"></script>
    <script src="../../js/moment.js"></script>
    <script src="../../js/bootstrap-datetimepicker.min.js"></script>

</body>

</html>
