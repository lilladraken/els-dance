<?php
  session_start();
  if (isset($_SESSION['authorised'])) {
    if ($_SESSION['authorised'] === false) {
      header('Location: ../../verify/');
      exit;
    }
  } else {
    header('Location: ../../verify/');
    exit;
  }
  $digital = false;
  if(isset($_POST['next'])){
    array_pop($_POST);
    $finalImages = $_POST;
  } else if(isset($_POST['digital'])){
    $digital = true;
    $finalImages = [];
  } else {
    $finalImages = [];
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="ELS School of Dance" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Stephanie Ridnell, Emma Scott">
  <link rel="apple-touch-icon" sizes="57x57" href="../../../img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="../../../img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../../../img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="../../../img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../../../img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="../../../img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="../../../img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="../../../img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../../../img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="../../../img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../../img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="../../../img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../../img/favicon/favicon-16x16.png">
  <link rel="manifest" href="../../../img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../../../img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title>ELS School of Dance</title>
  <link rel="stylesheet" href="../../../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../../../css/animate.css" />
  <link rel="stylesheet" href="../../../css/ionicons.min.css" />
  <link rel="stylesheet" href="../../../css/styles09022017.css" />
  <link rel="stylesheet" href="../../../css/bootstrap-datetimepicker.min.css" />
  <script src="../../../js/jquery-2.2.4.min.js"></script>
</head>
<body>
  <div class="contact-form">
    <center>
      <h2 class="cursive text-blue">ELS School of Dance</h2>
      <h4>Order Prints</h4>
    </center>
      <p class="small">Please review your order below then click submit.</p>
      <p class="small">Note that this is just an order. Emma will send you an
        invoice via email with payment options. Your order will not be filled
        until payment has been received in full.
      </p>
      <form name="confirmPhotoForm" class="form-horizontal" action="submit/" method="post">
        <input type="hidden" name="selected" id="selected" value="">
        <?php
          if (!$digital) {
            $index = 0;
            $num4x6 = 0;
            $num5x7 = 0;
            $subTotal = 0;
            $total = 0;
            foreach ($finalImages as $key=>$value) {
            if ($index % 3 === 0) {
              $num4x6 = $value;
              $subTotal = $num4x6 * 5;
            }
            if ($index % 3 === 1) {
              $num5x7 = $value;
              $image = substr($key, 0, -4);
              $image = str_replace('_', ' ', $image);
              $subTotal += $num5x7 * 7;
            }
            if ($index % 3 === 2) {
              $finish = $value;
              echo '
              <div class="row">
                <div class="col-sm-12">
                  <div class="card card-total horizontal">
                    <div class="card-image">
                      <img class="img-responsive" src="../../../img/concert_photos/'.$image.'.jpg">
                    </div>
                    <div class="card-stacked">
                      <div class="card-content">
                        4x6": '.$num4x6.'<br/>
                        5x7": '.$num5x7.'<br/>
                        Finish: '.$finish.'
                      </div>
                      <div class="card-action">
                        Sub Total: $'.$subTotal.'.00
                      </div>
                    </div>
                  </div>
                </div>
              </div>';
              $total += $subTotal;
            }
            $index++;
          }
        } else {
          echo '<div class="row">
            <div class="col-sm-12">
              <div class="card card-total horizontal">
                <div class="card-stacked">
                  <div class="card-content">
                    Digital copy of photos
                  </div>
                  <div class="card-action">
                    Sub Total: $50.00
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <input type="text" name="studentName" placeholder="Name of Student" required/>
            </div>
          </div>';
          $total = 50;
        }
        ?>
        <div class="row">
          <div class="col-sm-12">
            <div class="card card-total">
              <div class="card-content">
                Total: <span class="num" id="total">$<?php echo $total; ?>.00</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <input type="text" name="name" placeholder="Full Name" required/>
            <input type="email" name="email" placeholder="Email Address" required/>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <a href="../" class="btn btn-blue"><i class="ion-arrow-left-c"></i> Back</a>
          </div>
          <div class="col-xs-6 text-right">
            <button onclick="setValue()" name="submit" type="submit" class="btn btn-blue">Submit <i class="ion-arrow-right-c"></i></button>
          </div>
        </div>
      </form>
    </div>
    <script>
      function setValue() {
        var selectedPhotos = eval('(<?php echo json_encode($_POST)?>)');
        selectedPhotos = JSON.stringify(selectedPhotos);
        document.confirmPhotoForm.selected.value = selectedPhotos;
      }
    </script>
    <!--scripts loaded here for performance -->
    <script src="../../../js/bootstrap.min.js"></script>
    <script src="../../../js/jquery.easing.1.3.js"></script>
    <script src="../../../js/wow.js"></script>
    <script src="../../../js/scripts.js"></script>
    <script src="../../../js/moment.js"></script>
    <script src="../../../js/bootstrap-datetimepicker.min.js"></script>

</body>

</html>
