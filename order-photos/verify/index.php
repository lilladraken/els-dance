<?php
  session_start();
  $message = '';
  if (isset($_SESSION['wrongPassword'])) {
    if ($_SESSION['wrongPassword'] === true) {
      $message = 'Incorrect password.';
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="ELS School of Dance" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Stephanie Ridnell, Emma Scott">
  <link rel="apple-touch-icon" sizes="57x57" href="../../img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="../../img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../../img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="../../img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../../img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="../../img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="../../img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="../../img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../../img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="../../img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="../../img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../img/favicon/favicon-16x16.png">
  <link rel="manifest" href="../../img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../../img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title>ELS School of Dance - End of Year Concert Photos</title>
  <link rel="stylesheet" href="../../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../../css/animate.css" />
  <link rel="stylesheet" href="../../css/ionicons.min.css" />
  <link rel="stylesheet" href="../../css/styles09022017.css" />
  <script src="../../js/jquery-2.2.4.min.js"></script>
</head>
<body>
  <div class="contact-form">
    <center>
      <h2 class="cursive text-blue">ELS School of Dance</h2>
      <h4>2016 End of Year Concert Photos</h4>
    </center>
    <hr/>
    <div class="feature text-justify">
        <?php echo $message; ?>
        <form name="enrolmentForm" class="form-horizontal" action="../" method="post">
          <input type="password" placeholder="Enter Password" name="password" required>
          <input type="submit" name="submit" value="Submit"/>
        </form>
    </div>
  </div>
  <!--scripts loaded here for performance -->
  <script src="../../js/bootstrap.min.js"></script>
  <script src="../../js/jquery.easing.1.3.js"></script>
  <script src="../../js/wow.js"></script>
  <script src="../../js/scripts.js"></script>
</body>
</html>
