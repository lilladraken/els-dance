<?php
  session_start();
  session_destroy();
  session_start();
?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta name="google-site-verification" content="SuXEMrkNPOJ3lVqjTccV6FGW1YWlXjMT0jtkPV-nigw" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="ELS School of Dance" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Stephanie Ridnell, Emma Scott">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>ELS School of Dance</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/ionicons.min.css" />
    <link rel="stylesheet" href="css/styles09022017.css?v=894" />
  </head>

  <body>
    <div class="social-img">
      <a rel="nofollow" href="https://www.facebook.com/ELSdance" target="ext" title="Facebook">
        <img src="img/social/facebook.png" /> </a>
      <br/>
      <a rel="nofollow" href="https://www.instagram.com/elsschoolofdance/" target="ext" title="Instagram">
        <img src="img/social/Instagram-v051916.png" /> </a>
      <br/> </div>
    <a href="https://dancestudio-pro.com/online/index.php?account_id=7445" target="ext" rel="nofollow" class="btn btn-blue btn-enrol">Enrol Now!</a>
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="#top">
            <b class="els-font">ELS</b> School of Dance</a>
        </div>
        <div class="navbar-collapse collapse" id="bs-navbar">
          <ul class="nav navbar-nav">
            <li>
              <a class="page-scroll" href="#about">About</a>
            </li>
            <li>
              <a class="page-scroll" href="#meet">Teachers</a>
            </li>
            <li>
              <a class="page-scroll" href="#classes">Classes</a>
            </li>
            <li>
              <a class="page-scroll" href="#timetable">Timetable</a>
            </li>
            <li>
              <a class="page-scroll" href="#gallery">Gallery</a>
            </li>
            <li>
              <a class="page-scroll" href="#parties">Kids Parties</a>
            </li>
            <li>
              <a class="page-scroll" href="#uniform">Uniform</a>
            </li>
            <!-- <li>
            <a class="page-scroll" href="#merch">Merchandise</a>
          </li> -->
            <li>
              <a class="page-scroll" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <header id="top">
      <div id="headerVideo" class="collapse">
        <video preload="auto" autoplay="true" loop="loop" muted="true" class="fillWidth fadeIn wow" data-wow-delay="0.5s">
          <source src="img/ballet-vid.mp4" type="video/mp4">Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
      </div>
      <div class="header-content">
        <div class="header-title">
          <img class="logo-svg" src="img/logo.svg" />
          <br/>
        </div>
        <br/>
        <button type="button" class="btn btn-blue" data-toggle="collapse" data-target="#headerVideo">
          <i class="ion-ios-videocam"></i> Toggle Video</button>
      </div>
    </header>
    <!-- about -->
    <section id="about">
      <div class="container con-about">
        <div class="row">
          <div class="col-lg-6 text-justify">
            <center>
              <h2 class="margin-top-0 text-primary">About</h2>
            </center>
            <p>
              <span class="first-word">At ELS School of Dance,
                <a href="https://www.google.com.au/maps/place/ELS+School+of+Dance/@-37.7393553,144.824656,17z/data=!4m13!1m7!3m6!1s0x6ad65f02fd311841:0xfddb14519ffc3f25!2s13+Malcolm+Ct,+Kealba+VIC+3021!3b1!8m2!3d-37.7393553!4d144.8268447!3m4!1s0x6ad65f4c8f9c9309:0xd999431c1aa6ca21!8m2!3d-37.7399235!4d144.8264749"
                  rel="nofollow" target="ext">
                  Kealba, Melbourne</a>
              </span> - we are passionate about giving every child an equal opportunity to reach their full potential. We believe
              that dance is a great way to develop your child's fitness, attention span, co-ordination, balance and musicality.
            </p>
            <div class="card concert">
              <div class="card-image">
                <div class="videoWrapper">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/5xUa3ChFJQI" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <p>
              Dance is a great way to watch your child's personality and confidence come to life in a supportive environment with their
              friends.
            </p>
            <p>
              We also offer classes for adults; from those wanting to reignite their passion for dance to those who are looking for a fun
              way to get fit.
            </p>
            <p>
              We are continually focused on creating a welcoming and supportive environment for every indiviudal. We encourage and teach
              important life skills including respect, kindness, commitment, reliability, teamwork and encouragement to make
              our school feel like a second family for all staff, students and parents/families.
            </p>
            <center>
              <p>
                <span class="family">
                  We are more than just a dance school, we are a family.
                </span>
              </p>
            </center>
            <p>
              No matter your age, experience or goals our welcoming and friendly teachers are here to help you reach your full potential
              in a fun and supportive environment.
            </p>
            <p>
              Offering classes in all styles of dance for students
              <strong>ages 2 through to to adults</strong> we know you'll find a class you that suits you perfectly.
            </p>
            <center>
              <p>
                <span class="family">
                  Come join our dancing family today!
                </span>
              </p>
              <a href="https://dancestudio-pro.com/online/index.php?account_id=7445" target="ext" rel="nofollow" class="waves-effect waves-light btn btn-blue btn-lg wow flipInX">Enrol Now!</a>
              <br/>
              <br/>
              <br/>
            </center>
            <div class="card concert">
              <div class="card-image">
                <div class="videoWrapper">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/U_vfez2c4WU" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-lg-offset-1">
            <div class="card">
              <h3 class="class-card-heading">
                <i class="ion-calendar"></i> Important Dates</h3>
              <div class="card-content">
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      March 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      29
                    </div>
                    <div class="col-xs-3">
                      Thursday
                    </div>
                    <div class="col-xs-7">
                      Term 1 Ends
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      April 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      16
                    </div>
                    <div class="col-xs-3">
                      Monday
                    </div>
                    <div class="col-xs-7">
                      Term 2 Starts
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      June 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      30
                    </div>
                    <div class="col-xs-3">
                      Saturday
                    </div>
                    <div class="col-xs-7">
                      Term 2 Ends
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      July 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      16
                    </div>
                    <div class="col-xs-3">
                      Monday
                    </div>
                    <div class="col-xs-7">
                      Term 3 Starts
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      September 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      21
                    </div>
                    <div class="col-xs-3">
                      Friday
                    </div>
                    <div class="col-xs-7">
                      Term 3 Ends
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      October 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      8
                    </div>
                    <div class="col-xs-3">
                      Monday
                    </div>
                    <div class="col-xs-7">
                      Term 4 Starts
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      November 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      25
                    </div>
                    <div class="col-xs-3">
                      Sunday
                    </div>
                    <div class="col-xs-7">
                      BBQ Fundtraiser &amp; Photo Day
                    </div>
                  </div>
                </div>

                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      December 2018
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      7
                    </div>
                    <div class="col-xs-3">
                      Friday
                    </div>
                    <div class="col-xs-7">
                      Term 4 Ends
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      8
                    </div>
                    <div class="col-xs-3">
                      Saturday
                    </div>
                    <div class="col-xs-7">
                      Annual End of Year Concert
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                      9
                    </div>
                    <div class="col-xs-3">
                      Sunday
                    </div>
                    <div class="col-xs-7">
                      Christmas Party &amp; Presentation Day
                    </div>
                  </div>
                </div>
                <div class="container important-dates">
                  <div class="row month">
                    <div class="col-xs-12">
                      TBC
                    </div>
                  </div>
                  <div class="row headings">
                    <div class="col-xs-2">
                      Date
                    </div>
                    <div class="col-xs-3">
                      Day
                    </div>
                    <div class="col-xs-7">
                      Event
                    </div>
                  </div>
                  <div class="row dates">
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-3">
                    </div>
                    <div class="col-xs-7">
                      Concert Rehearsals
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br/>
            <br/>
            <div class="card">
              <h3 class="class-card-heading">
                <i class="ion-heart"></i> Giving Back</h3>
              <div class="card-image text-center">
                <center>
                  <br/>
                  <img class="img-responsive" src="img/beyond_blue.jpg" width="150">
                </center>
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  At ELS School of Dance we are proud to give back to the community and hope to continue to as our school grows. Our 2017 Concert
                  raffle raised $320 which we are proud to have donated to Beyond Blue.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-image text-center">
                <center>
                  <br/>
                  <img class="img-responsive" src="img/footy_colours_day.png" width="150">
                </center>
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  Footy Colours week - we raised $110 to help young learners continue with their education while they receive treatment for
                  cancer.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-image">
                <center>
                  <img class="img-responsive" src="img/taras_dream.png" width="150">
                </center>
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  Last year our concert raffle raised $312 to the "
                  <a href="https://www.facebook.com/tarasdreamfoundation/" target="ext" rel="nofollow">Tara's Dream Foundation</a>". A big thank you goes out to our families who helped make this possible!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- testimonials 1 -->
    <aside id="testimonials_1" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE MUMS ARE SAYING </p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote">
                </i>My daughter started this year. I can already see huge improvements after 1 term. Miss Nicole is fantastic
                with the kids. Little miss cannot wait every week for her class.</p>
              <p class="text-faded small wide-space">-Georgia</p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- the team -->
    <section id="meet">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="margin-top-0 text-primary">The Teachers</h2>
            <br/>
            <br/>
          </div>
        </div>
      </div>
      <div class="container text-center">
        <div class="row">
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Emma.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Miss Emma Scott</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Certificate IV in Dance</p>
                  <p class="text-muted teacher">Diploma of Dance (Elite Performance)</p>
                  <p class="text-muted teacher">Qualified Kinderballet Teacher</p>
                  <p class="text-muted teacher">Correctalign Pilates Instructor.</p>
                </div>
                <div class="card-action">
                  <a href="#aboutEmma" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Miss Nicole Kaminski</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Certificate II in Musical Theatre</p>
                  <p class="text-muted teacher">Bachelor of Music Theatre</p>
                </div>
                <div class="card-action">
                  <a href="#aboutNicole" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Nicole.jpg">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Mon.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Miss Mon Heras</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Certificate IV in Dance Teaching</p>
                  <p class="text-muted teacher">Qualified Kinderballet Teacher</p>
                  <p class="text-muted teacher">Qualified Yoga Instructor</p>
                </div>
                <div class="card-action">
                  <a href="#aboutMon" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Samantha Black</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Certificate IV in Personal Training &amp; Fitness / Master Trainer (AIF)</p>
                  <p class="text-muted teacher">Post/Prenatal Training Certified</p>
                  <p class="text-muted teacher">Certified in Rehab Training</p>
                </div>
                <div class="card-action">
                  <a href="#aboutSam" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Sam.jpg">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Caitlyn.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Miss Caitlin Smith</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Certificate IV in Dance</p>
                  <p class="text-muted teacher">Diploma of Dance (Elite Performance)</p>
                  <p class="text-muted teacher">Accredited Cheerleading and Tumbling coach - All Star Cheerleading Federation (AASCF)</p>
                </div>
                <div class="card-action">
                  <a href="#aboutCaitlin" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Miss Stephanie Gardiner</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Cert IV in Dance</p>
                  <p class="text-muted teacher">Diploma in Dance (Elite Performance)</p>
                </div>
                <div class="card-action">
                  <a href="#aboutSteph" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Steph.jpg">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card horizontal">
              <div class="card-image">
                <img class="img-responsive" src="img/teachers/Andrea.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <span class="card-title">Miss Andrea San Diego</span>
                  <hr class="card-hr" />
                  <br/>
                  <p class="text-muted teacher">Cert II in Dance</p>
                </div>
                <div class="card-action">
                  <a href="#aboutAndrea" class="btn btn-blue wow flipInX" data-toggle="modal">Bio</a>
                  <br/>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <!-- testimonials 2 -->
    <aside id="testimonials_2" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE MUMS ARE SAYING</p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote"></i>
                This is Abbey's first term of dancing and it won't be her last. Dancing makes Abbey happy and the way Miss Emma interacts
                with all the kids in her class is very special to watch. She provides them with confidence and embraces their
                skills and individualism. Well done ladies!</p>
              <p class="text-faded small wide-space">-Pauline</p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- classes -->
    <section id="classes">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="margin-top-0 text-primary">Classes</h2>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="masonry">
          <!-- ballet -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/ballet.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Ballet</h3>
                <div class="card-content">
                  Classical Ballet classes are highly recommended for all students as ballet technique is the core to every dance style. It
                  teaches good posture, strong technique, flexibility and discipline. Ballet as well as all styles helps
                  boost your child's focusing ability and memory.
                </div>
              </div>
            </div>
          </div>
          <!-- toddler -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/toddler.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Toddler Ballet &amp; Movement to Music</h3>
                <div class="card-content">
                  Starting your little one young is the perfect way to introduce them into a structured learning environment. Teachers use
                  age appropriate music as well as interactive story telling and props to keep the littlies engaged and make
                  sure they are having lots of fun. This class helps build your little one's confidence, focus and most importantly;
                  their friendships. Our class will teach your child how to listen and follow the teacher's instructions
                  along with the basic dance fundamentals.
                </div>
              </div>
            </div>
          </div>
          <!-- musical theatre / acting -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/msuical_theatre.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Musical Theatre</h3>
                <div class="card-content">
                  During our musical theatre classes students learn acting, singing and dancing techniques all in the one class. This class
                  is super fun, students get to use their imagination and creativity while learning new skills.
                </div>
              </div>
            </div>
          </div>
          <!-- cheerleading -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/acro_cheerleading.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Cheerleading and Acrobatics</h3>
                <div class="card-content">
                  Cheerleading and Acrobatics is so much fun and is a great class to grow your child's confidence. Students works as a team
                  and learn acrobatics, tumbling and cheerleading lifts as well as dance fundamentals and techniques.
                </div>
              </div>
            </div>
          </div>
          <!-- hip hop -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/hiphop.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Hip Hop</h3>
                <div class="card-content">
                  Hip Hop is a great style to shake up your child's skills by learning fun and funky choreography to music they love while
                  inproving the fitness, confidence, memory and focus levels all at the same time.
                </div>
              </div>
            </div>
          </div>
          <!-- contemporary -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/contemp.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Contemporary / Lyrical</h3>
                <div class="card-content">
                  Contemporary and Lyrical is a style of expressive dance that combines elements of several dance genres. The Lyrical element
                  in our class focuses on students connecting to the story line of their routines and telling the audience
                  a story with their expression and performance.
                </div>
              </div>
            </div>
          </div>
          <!-- jazz -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/jazz.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Jazz</h3>
                <div class="card-content">
                  Our Jazz classes are fun, upbeat classes that teach our students strong dance technique, strength and flexibility while learning
                  sharp, strong movements and choreography.
                </div>
              </div>
            </div>
          </div>
          <!-- ballroom -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/ballroom.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Ballroom</h3>
                <div class="card-content">
                  Ballroom dance is a great class for fitness, confidence and fun. It is is a great class to improve coordination, rhythm and
                  learn different dance techniques. We cover styles of Cha Cha, Salsa, Merengue and Tango just to name a
                  few. All styles are different but we focus on precision, technique and different rhythms. These classes
                  are a super fun way to get you moving out on the dance floor solo or partnering. It’s a fun new way to
                  get moving.
                </div>
              </div>
            </div>
          </div>
          <!-- tap -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/tap.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Tap</h3>
                <div class="card-content">
                  Tap is a fun style of dance which focuses on rhythm and musicality as well as technique. This class helps build your child's
                  focus and co-ordination while having lots of fun!
                </div>
              </div>
            </div>
          </div>
          <!-- vocal group -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/vocal.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Vocal Group</h3>
                <div class="card-content">
                  If your child loves to sing, this is the class for them! In Vocal Group your child will learn multiple singing techniques
                  and learn to sing in a range on styles individually and as a group. This class is lots of fun and is a
                  great class to help get your child out of their shell!
                </div>
              </div>
            </div>
          </div>
          <!-- strength and stretch -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/strengthstretch.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Stretch &amp; Strength</h3>
                <div class="card-content">
                  Our stretch and strength class is designed for students aged 9+ and is highly recommended! The class is designed to help
                  students strengthen their bodies and increase their flexibility using techniques which will help their
                  dancing. Students also learn about the way their bodies work which helps them focus on correct posture
                  and alignment and will help prevent injury.
                </div>
              </div>
            </div>
          </div>
          <!-- adult classes -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/adult.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Adult Classes</h3>
                <div class="card-content">
                  Whether you are someone who is looking for a new, fun way to get fit or you are wanting to reignite your childhood passion
                  for dance our adults classes are for you. You will see on our timetable that we offer a range of dance
                  and fun group fitness classes. Our classes are designed for beginners through to those who are more advanced.
                  No standards or age limits apply!
                </div>
              </div>
            </div>
          </div>
          <!-- competition classes -->
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/classes/competition.jpg">
              </div>
              <div class="card-stacked">
                <h3 class="class-card-heading">Competition Classes</h3>
                <div class="card-content">
                  Is your child wanting to take dance a bit more seriously? We currently offer one on one dance or singing lessons, in all
                  styles. During these classes students learn solos to perform at competitions and fun community events where
                  they are supported by their teachers and peers. This gives them the opportunity to perform up on the stage
                  and meet other dancers from across Melbourne. Our competition students gain a world of confidence up on
                  stage and have lots of fun. I highly recommend competition classes if you have a child who loves to perform.
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <!-- testimonials 3 -->
    <aside id="testimonials_3" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE MUMS ARE SAYING</p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote"></i>
                The team at ELS School are just amazing. My two daughters absolutely love it here. Thank you for making my little girls dreams
                come true and turning them into a ballerina and a little hip hop dancer. We are so grateful to have found
                this school we are excited to see how the girls will grow and develop with their dance.
              </p>
              <p class="text-faded small wide-space">-Shannah
              </p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- timetable -->
    <section id="timetable">
      <div class="container">
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <h2 class="text-center text-primary">Timetable</h2>
            <center>
              <a href="docs/timetable/2018/Adult Timetable 20180302.pdf" target="ext" class="btn btn-blue wow flipInX">
                Adult Timetable
              </a>
              <br/>
              <br/>
              <a href="docs/timetable/2018/Children Timetable 20180302.pdf" target="ext" class="btn btn-blue wow flipInX">
                Children Timetable
              </a>
              <br/>
              <br/>
              <a href="docs/timetable/2018/Preschool Toddler Timetable 20180302.pdf" target="ext" class="btn btn-blue wow flipInX">
                Preschool Toddler Timetable
              </a>
              <br/>
              <hr class="dark" />
            </center>
          </div>
        </div>
        <div class="row">
          <!-- age groups -->
          <div class="col-lg-6 col-md-6 wow fadeIn">
            <div class="feature class-card">
              <h3 class="class-card-heading">Age Groups</h3>
              <div class="class-card-body">
                <h4 class="text-blue">Toddler</h4>
                <div class="media-body media-middle">
                  <p>1.5 - 3 yrs
                    <p class="small">(parents are to participate with their children during class)</p>
                </div>
                <h4 class="text-blue">Preschool</h4>
                <div class="media-body media-middle">
                  <p>3 - 4 years</p>
                </div>
                <h4 class="text-blue">Beginner</h4>
                <div class="media-body media-middle">
                  <p>4.5 - 5 years</p>
                </div>
                <h4 class="text-blue">Junior</h4>
                <div class="media-body media-middle">
                  <p>6 - 7 years</p>
                </div>
                <h4 class="text-blue">Intermediate</h4>
                <div class="media-body media-middle">
                  <p>8 - 9 years</p>
                </div>
                <h4 class="text-blue">Presenior</h4>
                <div class="media-body media-middle">
                  <p>10 - 12 years</p>
                </div>
                <h4 class="text-blue">Senior</h4>
                <div class="media-body media-middle">
                  <p>13+</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 wow fadeIn">
            <div class="feature class-card">
              <h3 class="text-center class-card-heading">Fees</h3>
              <div class="class-card-body">
                <p>Once enroled, classes are paid per term.*</p>
                <p class="text-blue">Parents are invited to attend their child's first class as well as the open days held at the end of each
                  term.
                </p>
                <br/>
                <div class="fee-table">
                  <div class="row">
                    <div class="col-xs-8 fee-label">
                      Annual admininistration and insurance fee:
                    </div>
                    <div class="col-xs-4 fee-item">
                      $25 (per family)
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-8 fee-label">
                      45min classes:
                    </div>
                    <div class="col-xs-4 fee-item">
                      $14
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-8 fee-label">
                      60min classes:
                    </div>
                    <div class="col-xs-4 fee-item">
                      $16
                    </div>
                  </div>
                </div>
                <p>We also have Class Package deals and discounts available for students who attend multiple classes – the more
                  classes your child does, the more you save!
                </p>
                <br/>
                <center>
                  <a href="docs/ClassPackageDeals.pdf" target="ext" class="btn btn-blue wow flipInX">
                    <i class="ion-android-download"></i> Download Package Deals</a>
                </center>
                <br/>
                <p class="text-blue small">PRIVATE SINGING, DANCING, COMPETITION AND WEDDING DANCE LESSONS ARE AVAILABLE UPON REQUEST.</p>
              </div>
              <p class="small">*Extra fees apply if your child participates in our end of year concert. While participation is not compulsory,
                we strongly encourage all students to be involved in our exciting end of year event!</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
            <br/>
            <hr class="dark" />
            <center>
              <a href="docs/schoolpolicy" target="ext" class="btn btn-blue wow flipInX">
                <i class="ion-search"></i> View School Policy</a>
            </center>
            <hr class="dark" />
            <p class="small">All classes run in conjunction with the Victorian school term dates.</p>
            <p class="small">Classes DO NOT run on Public Holidays.</p>
            <p class="small">Make up classes are available if a class falls on a public holiday, a concert rehearsal or if a child misses
              a class for any reason (with in the same term).</p>
            <p class="small">Refunds are not avalible unless it's due to serious / long term injury or illness.</small>
          </div>
        </div>
      </div>
    </section>
    <!-- testimonials 4 -->
    <aside id="testimonials_4" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE DANCERS ARE SAYING</p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote"></i>
                [My son] loves going to hip hop every week. Thank you Miss Emma for being an amazing teacher. See you in term 2!!</p>
              <p class="text-faded small wide-space">-Melissa</p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- gallery -->
    <section id="gallery" class="no-padding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 text-center">
            <br/>
            <br/>
            <h2 class="margin-top-0 text-primary">Gallery</h2>
            <p class="small">(Click photos to zoom)</p>
          </div>
        </div>
        <br/>
        <?php
          $scanDIR = scandir('./img/gallery');
          $images = array();
          foreach($scanDIR as $key=>$value) {
            if (strlen($value) > 4) {
              $image = $value;
              list($width, $height) = getimagesize('./img/gallery/'.$image);
              if ($width <= $height) {
                $thumb =
                '<div class="thumb">
                  <img src="img/gallery/'.$image.'" class="portrait" alt="Image" />
                </div>';
              } else {
                $thumb =
                '<div class="thumb">
                <img src="img/gallery/'.$image.'" class="landscape" alt="Image" />
                </div>';
              }
              $images[] =
              '<a href="#galleryModal" class="gallery-box" data-toggle="modal" data-src="img/gallery/'.$image.'">'.$thumb;
            }
          }
          echo '<div class="row no-gutter">';
          foreach ($images as $index=>$image) {
            echo '
            <div class="col-lg-2 col-sm-4 col-xs-6">
              '.$image.'
                <div class="gallery-box-caption">
                  <div class="gallery-box-content">
                    <div>
                      <i class="icon-lg ion-ios-search"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>';
          }
          echo '</div>';
        ?>
          <br/>
          <div class="container text-center">
            <div class="call-to-action">
              <br/>
              <br/>
              <i class="icon-lg ion-social-instagram"></i>
              <br/>
              <a href="https://www.instagram.com/elsschoolofdance/" target="ext" class="btn btn-lightbg wow flipInX">See More</a>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/> </div>
          </div>
      </div>
      </div>
    </section>
    <!-- testimonials 5 -->
    <aside id="testimonials_5" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE MUMS ARE SAYING</p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote"></i>
                Thank you Miss Emma from ELS School of Dance! My daughter attends beginner ballet classes and she LOVES it!! It was so great
                to see her during open week and see how far she has come since last year and her progress during term 1!
                Most importantly your dedication, caring nature and love towards each student is now the foundation of a
                family dance school and that is all credit to you.</p>
              <p class="text-faded small wide-space">-Denise
              </p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- kids parties -->
    <section id="parties" class="no-padding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <br/>
            <br/>
            <center>
              <h2 class="margin-top-0 text-primary">Kids Parties</h2>
            </center>
            <br/>
            <div class="col-sm-8 col-sm-offset-2">
              <div class="card">
                <div class="class-card-body">
                  <p>
                    At ELS School of Dance we offer fun, magical and very special party packages that your child and their friends will never
                    forget. Our party packages come in a variety of themes, including Ballerina, Hip Hop, Pop Star and Fairy
                    themed parties. Our party packages will be perfect for children's birthdays, christenings, baptisms,
                    Christmas parties and other special events.
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4 col-xs-12">
                  <div class="card feature">
                    <img src="img/parties/party1.jpg" class="img-responsive" alt="ELS Kids Parties" /> </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                  <div class="card feature">
                    <img src="img/parties/party3.jpg" class="img-responsive" alt="ELS Kids Parties" /> </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                  <div class="card feature">
                    <img src="img/parties/party2.jpg" class="img-responsive" alt="ELS Kids Parties" /> </div>
                </div>
              </div>
              <div class="card">
                <div class="class-card-body">
                  <h4>
                    1.5hr booking - $190.00
                  </h4>
                  <p>
                    This includes a party theme of your choice, pass the parcel (prize for each child), your choice between face painting and
                    ballooning, magic tricks, 20 min dance class (style of your choice), a range of fun interactive games
                    including musical statues, limbo, parachute game and singing.
                  </p>
                  <br/>
                  <h4>
                    2hr booking - $250.00
                  </h4>
                  <p>
                    This includes a party theme of your choice, pass the parcel (prize for each child), face painting, ballooning, magic tricks,
                    20 min dance class (style of your choice), a range of fun interactive games including; musical statues,
                    limbo, parachute game and singing.
                  </p>
                  <p class="text-muted">
                    Please know that our party packages are based on a max of 20 children. If there are over 20 children the party packages may
                    vary.
                  </p>
                  <p class="text-muted">
                    We can also come to you for an additional $10.00 (if you are more than 30 mins away from ELS School of Dance this travel
                    fee will increase).
                  </p>
                </div>
              </div>
              <div class="card">
                <div class="class-card-body">
                  <h4>
                    Food
                  </h4>
                  <p>
                    If you would like to bring food, cake etc to ELS School of Dance for your child's party you are welcome to book in an extra
                    half an hour for the children to eat, sing happy birthday etc. This will cost an extra $30.00 to cover
                    chair and table set up, room hire and extra clean up.
                  </p>
                  <h4>
                    Promotional Events
                  </h4>
                  <p>
                    We are also happy to adjust our party packages for shopping centers and other promotional events where there is no set number
                    of children (eg. roving balloonist for a grand opening).
                  </p>
                </div>
              </div>
              * Decorations not included
            </div>
          </div>
        </div>
        <br/>
        <br/>
      </div>
    </section>
    <!-- testimonials 6 -->
    <aside id="testimonials_6" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE MUMS ARE SAYING</p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote"></i>
                It was the best decision I made for my daughter to join ELS School of Dance. Both Miss Emma and Miss Nicole are not only
                talented but so lovely and kind. My daughter has never been happier and it has been so good for her in every
                way. Watching at the end of term and witnessing how much my daughter has gained confidence and is just so
                happy to get involved made my day. Thank you Miss Emma and Miss Nicole!
              </p>
              <p class="text-faded small wide-space">-Belinda
              </p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- uniform -->
    <section id="uniform">
      <div class="container">
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <h2 class="text-center text-primary">Uniform</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 text-justify">
            <div class="card feature">
              <img src="img/uniform_1.jpg" class="img-responsive" alt="ELS Uniform" /> </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="card feature">
              <div class="class-card-body">
                <p class="text-muted">
                  The ELS uniform can be purchased from our uniform shop at the studio during specified opening hours.
                </p>
                <br/>
                <p class="text-muted">
                  <strong>Second hand uniforms</strong>
                </p>
                <p class="text-muted">
                  There are also second hand uniforms available. Whether buying or selling, please send any enquiries to
                  <br/>
                  <a href="mailto:nicole@els-dance.com?subject=Second%20hand%20uniforms">
                    nicole@els-dance.com
                  </a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="card feature">
              <img src="img/uniform_2.jpg" class="img-responsive" alt="ELS Uniform" /> </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class="feature">
              <center>
                <h2 class="cursive">Ballet</h2>
              </center>
              <div class="card">
                <div class="class-card-body">
                  <p class="text-muted">Once enrolled all female ballet students should purchase an electric blue leotard with theatrical pink
                    ballet stockings and an electric blue skirt. </p>
                  <br/>
                  <p class="text-muted">For the boys please purchase an electric blue singlet and black tights. Ballet shoes are essential to all
                    ballet classes. </p>
                  <br/>
                  <p class="text-muted small">Toddler classes have no set uniform however ballet shoes are recommended.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="feature">
              <center>
                <h2 class="cursive">Hip Hop / Cheerleading / Acrobatics</h2>
              </center>
              <div class="card">
                <div class="class-card-body">
                  <p class="text-muted">
                    For Hip Hop and Cheerleading classes children should wear an electric blue top that they are comfortable in, with black shorts
                    or pants. Runners/sneakers should be worn to all Hip-hop and Cheerleading classes.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="feature">
              <center>
                <h2 class="cursive">Other</h2>
              </center>
              <div class="card">
                <div class="class-card-body">
                  <p class="text-muted">For Contemporary/Lyrical, Jazz and Musical Theatre classes your child should be wearing either an electric
                    blue leotard or tight fitting t-shirt/singlet/crop top with tight black shorts or tights. </p>
                  <br/>
                  <p class="text-muted">Foot thongs/undies must be worn for Contemporary. </p>
                  <br/>
                  <p class="text-muted">For Jazz and Musical Theatre please come with black slip on Jazz shoes. </p>
                  <br/>
                  <p class="text-muted">For Tap please wear tanned (girls) or black (boys) tap shoes. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <h2 class="text-center text-primary">Merchandise</h2>
          </div>
        </div>
        <div class="masonry">
          <div class="item">
            <div class="card">
              <div class="card-content">
                <h4>Talk to one of our friendly staff members at ELS School of Dance to purchase any of our merchandise!</h4>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/merch/dance_bag.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <h3 class="class-card-heading">Large Bag: $45.00</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/merch/bag.png">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <h3 class="class-card-heading">Small Bag: $12.00</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/merch/bumper sticker.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <h3 class="class-card-heading">Bumper Sticker: $1.00</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/merch/drinkbottle.png">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <h3 class="class-card-heading">Drink Bottle: $12.00</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/merch/jackets.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <h3 class="class-card-heading">Jackets: $50.00</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card">
              <div class="card-image">
                <img class="img-responsive" src="img/merch/merch.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <h3 class="class-card-heading">Hot Deal</h3>
                  Bag, drink bottle and bumper stick for $20.00!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- testimonials 7 -->
    <aside id="testimonials_7" class="bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-justify">
            <div class="feature-about">
              <p class="text-faded small wide-space">WHAT THE MUMS ARE SAYING</p>
              <br/>
              <p class="text-faded wide-space">
                <i class="icon-quote ion-quote"></i> Both Miss Emma and Miss Nicole are AMAZING! This is more than a dance school, it's a little dance family.
                My daughter loves attending her Musical Theatre class. Everyone is so warm and friendly. Highly recommend
                it!!
              </p>
              <p class="text-faded small wide-space">-Lucianna
              </p>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <!-- contact -->
    <aside id="contact" class="aside-contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h2 class="margin-top-0 wow fadeIn text-center text-primary">Contact Us</h2>
            <div class="contact-form">
              <div class="row details">
                <div class="col-sm-6">
                  <span class="contact-letter">A</span> 13 Malcolm Court
                  <br/> &nbsp;&nbsp;&nbsp;&nbsp;Kealba
                  <br/> &nbsp;&nbsp;&nbsp;&nbsp;VIC 3021</br>
                  </br>
                </div>
                <div class="col-sm-6">
                  <span class="contact-letter">E</span> info@els-dance.com
                  <br/>
                  <br/>
                  <span class="contact-letter">P</span> 0417 127 467
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-10 col-lg-offset-1 text-center">
          <div class="contact-form">
            <form name="contactForm" class="form-horizontal" action="contact/" method="post">
              <input type="text" name="name" placeholder="Your Name" />
              <input type="email" name="email" placeholder="Email Address" />
              <textarea name="message" placeholder="Type your Message"></textarea>
              <input type="submit" name="submit" value="Send" />
            </form>
          </div>
          <br/> </div>
      </div>
      </div>
    </aside>
    <!-- footer -->
    <footer id="footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-6 column">
            <h4 class="cursive">Connect</h4>
            <ul class="list-inline">
              <li>
                <a class="social" rel="nofollow" href="https://www.facebook.com/ELSdance" target="ext" title="Facebook">
                  <i class="icon-lg ion-social-facebook"></i>
                </a>&nbsp;</li>
              <li>
                <a class="social" rel="nofollow" href="https://www.instagram.com/elsschoolofdance/" target="ext" title="Instagram">
                  <i class="icon-lg ion-social-instagram-outline"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-xs-6 text-right">
            <h4 class="cursive">Links</h4>
            <span class="small">
              <a href="docs/schoolpolicy" class="footer-link">School Policy</a>
            </span>
            <br/>
            <span class="small">
              <a href="docs/childsafepolicy" class="footer-link">Child Safe Policy</a>
            </span>
            <br/>
            <span class="small">
              <a href="docs/teachercodeofconduct" class="footer-link">Teachers Code of Conduct</a>
            </span>
            <br/>
            <span class="small">
              <a href="docs/timetable/2018/Adult Timetable 2018.pdf" target="ext" class="footer-link">Adult Timetable</a>
            </span>
            <br/>
            <span class="small">
              <a href="docs/timetable/2018/Children Timetable 2018.pdf" target="ext" class="footer-link">Children Timetable</a>
            </span>
            <br/>
            <span class="small">
              <a href="docs/timetable/2018/Preschool Toddler Timetable 2018.pdf" target="ext" class="footer-link">Preschool Toddler Timetable</a>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <p class="small">ELS School of Dance operates under child safety policies and practises. If you feel anything breaches these standards
              please call 000 or contact the ELS Staff immediately.</p>
            <small>Copyright &copy;
              <?php echo date("Y"); ?> ELS School of Dance. All rights reserved.</small>
          </div>
        </div>
        <br/> </div>
    </footer>
    <!-- modal windows -->
    <!-- template for gallery windows -->
    <div id="galleryModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-body">
            <img src="img/logo.svg" id="galleryImage" class="img-responsive" />
            <p>
              <br/>
              <center>
                <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                  <i class="ion-android-close"></i>
                </button>
              </center>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- about emma window -->
    <div id="aboutEmma" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Miss Emma</h2>
            <div class="feature text-justify">
              <p>The director and founder of ELS School of Dance, Emma Scott, is a classically-trained ballet dancer who has
                studied in the RAD syllabus since the age of four. </p>
              <br/>
              <p>As well as Ballet, Emma developed a passion for contemporary dance at a young age, which lead her to perform
                in the Tasmanian Tour with Tas Dance in 2010. </p>
              <br/>
              <p>Since then Emma has trained with Jason Coleman at Ministry of Dance in ballet, contemporary, jazz, cheerleading,
                lyrical, hip-hop, commercial jazz, musical theatre and acting. She then graduated in 2014 as a professional
                dancer with a certificate IV in Dance and a Diploma in Dance/Elite Performance. </p>
              <br/>
              <p>Emma's experiences have lead her to develop an ardor for teaching and helping others find joy through dance.
                She has taught and instructed many classes and workshops at multiple schools around Melbourne, Victoria and
                Tasmania, to children as young as two all the way through to adults. </p>
              <br/>
              <strong>Emma also possesses:</strong>
              <br/>
              <br/>
              <p>Working With Children Check</p>
              <p>First Aid Certificate</p>
              <p>Current CPR Certificate</p>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- about nicole window -->
    <div id="aboutNicole" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Miss Nicole</h2>
            <div class="feature text-justify">
              <p>With a dance teacher for a mother, Nicole’s love of performance sprouted early. In the years that followed,
                she discovered that theatre was the direction she wanted to take and build her skills. Nicole completed a
                Certificate II in Musical Theatre at WAAPA, and last year completing her BA Musical Theatre at Federation
                University Ballarat, she was lucky enough to play the title role of Millie in Thoroughly Modern Millie, amongst
                various other performances while undergoing her studies. Nicole has also performed under the direction of
                Australian theatre legend, Nancye Hayes, and Australian composer Matthew Lee Robinson, which the hope that
                these are just the start of the incredible Australian art producers that she has the privilege of working
                with in the future. </p>
              <br/>
              <p>As well as being an incredible performer, Nicole is an experienced and passionate teacher who we are lucky
                to have in our ELS family. </p>
              <br/>
              <strong>Nicole also possesses:</strong>
              <br/>
              <br/>
              <p>Working With Children Check</p>
              <p>First Aid Certificate</p>
              <p>Current CPR Certificate</p>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- about caitlin window -->
    <div id="aboutCaitlin" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Miss Caitlin</h2>
            <div class="feature text-justify">
              <p>Growing up training in Ballet Jazz and Tap since she was three Caitlin then found a passion in Acro Dance and
                Tumbling when she was eighteen. This lead Caitlin to the competitive world of All Star Cheerleading where
                she trained under some of Australia's top ranked coaches, her Cheerleading team now has 4 national titles
                under their belt.</p>
              <br/>
              <p>Caitlin also trained at Jason Coleman's Ministry of Dance in ballet, jazz, hip hop, contemporary, tap, acrobatics,
                musical theatre and more. After her training she graduated with her Cert IV in Dance and Diploma in Dance
                (elite performance).</p>
              <br/>
              <p>Caitlin found her love for working with children when she stared working in child care in 2010. Her love of
                dance and working with children has lead her to become a passionate and great teacher.</p>
              <br/>
              <strong>Caitlin also possesses:</strong>
              <br/>
              <br/>
              <p>Diploma in Children's Services</p>
              <p>Working With Children's Check</p>
              <p>Level 2 First Aid Certificate</p>
              <p>Current CPR Certificate</p>
              <p>First Aid Management of Anaphylaxis</p>
              <p>Management of Asthma</p>
              <br/>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- about mon window -->
    <div id="aboutMon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Miss Mon</h2>
            <div class="feature text-justify">
              <p>
                Mon’s first language is movement as she started ballet at the age of three. She finished her RAD ballet vocational exams
                through to Advanced 2 and then completed short courses in London. Later she expanded her training in the
                style of contemporary where she discovered her new passion and studied full time dance at Broadway Dance
                Centre with a scholarship. Pursuing her love for teaching, Mon completed a Certificate IV in Dance Teaching
                at Transit Dance and to compliment her knowledge in body alignment and connection with the mind-body she
                became a certified yoga instructor. She has since found her voice through dance and aims to encourage people
                find theirs.
              </p>
              <br/>
              <strong>Mon also possesses:</strong>
              <br/>
              <br/>
              <p>Working With Children's Check</p>
              <p>First Aid Certificate</p>
              <p>Current CPR Certificate</p>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- about sam window -->
    <div id="aboutSam" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Samantha Black</h2>
            <div class="feature text-justify">
              <h4>Boot Camp Fitness Classes with Sam</h4>
              <p>
                Combining a mix of HIIT, CIRCUIT,STRENGTH &amp; CARDIO.
              </p>
              <br/>
              <p>
                Samantha from She-Wolf Fitness has created a perfect combination that gets you up and moving through an assortment of interval
                training and bodyweight exercises, aimed at getting you the results you want, and fast.
              </p>
              <br/>
              <p>
                Samantha is a dedicated Mum of 3, Personal Trainer &amp; Fitness Instructor. Her qualification's are in pre/postnatal &amp;
                rehab training &amp; she has a passion for fun, yet functional group workouts, which will see you achieve
                maximum results.
              </p>
              <br/>
              <p>
                Her classes are aimed at weight loss and toning, for all levels of fitness - from Beginners to Advanced, our full body functional
                classes, are different each session, keeping it fresh and fun and also tailored to the needs of each member
                in the group.
              </p>
              <br/>
              <p>
                From your Functional Fitness class you will get a full body workout which will include the best toning and cardio exercises
                for ultimate results.
              </p>
              <br/>
              <p>
                You can expect to increase your strength, tone your core and improve your fitness all while having fun!
              </p>
              <br/>
              <p>
                Classes are child friendly and suitable for Individuals aged 18+
              </p>
              <br/>
              <p>
                Outdoor classes also available.
              </p>
              <br/>
              <p>
                One-on-one PT sessions also available upon request.
              </p>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- about steph window -->
    <div id="aboutSteph" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Miss Stephanie</h2>
            <div class="feature text-justify">
              <p>
                Stephanie has been dancing since the age of 3 and has forever since had a passion in dance. Stephanie studied through the
                Commonwealth Society of Teachers of Dance where she was awarded the Perpetual Gold Star Trophy for Tap, ranking
                her the highest scoring tap dancer of 2013.
              </p>
              <br/>
              <p>
                From there, Stephanie went on to train at Jason Coleman’s Ministry of Dance, obtaining a Certificate IV in Dance Performance
                and soon after was offered a scholarship to train at The Edge School of Dance to complete her Diploma in
                Elite Performance.
              </p>
              <br/>
              <p>
                In 2016, Stephanie lived in Japan for 13 months to perform at Tokyo Disneyland where she was able to perform as some of our
                favourite Disney Princesses and perform in all the wonderful Disney Parades.
              </p>
              <br/>
              <p>
                Since returning back to Australia in 2017, Stephanie wishes to focus her passion of dance through her teaching in hopes to
                inspire and educate the younger generation and allowing them to find their passion in dance as well.
              </p>
              <br/>
              <strong>Stephanie also possesses:</strong>
              <br/>
              <br/>
              <p>Working With Children's Check</p>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- about a ndrea window -->
    <div id="aboutAndrea" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2 class="text-center text-blue cursive">Miss Andrea</h2>
            <div class="feature text-justify">
              <p>
                Ever since taking her first jazz class in year 7, Andrea has had a strong passion for dance. From there her training grew
                into doing ballet and contemporary classes and eventually taking hiphop classes which she fell in love with.
              </p>
              <br/>
              <p>
                She has participated in national and regional eisteddfods such as Royal South street, Swan Hill, Ararat and Bendigo eisteddfods.
                Doing VET VCE dance for two years, has gained her Certificate 2 in Dance, getting one of the top marks in
                Victoria.
              </p>
              <br/>
              <p>
                Andrea is currently training as a full-time student at Jason Coleman’s Ministry of Dance studying jazz, contemporary, hiphop,
                ballet, acro, tap, latin, heels and broadway jazz where she is working towards her Diploma in Dance (Elite
                Performance).
              </p>
              <br/>
              <p>
                Encouraging students self-expression through dance is one of her goals in teaching. She believes that promoting a fun, challenging
                and creative environment will not only help students in class but also in life.
              </p>
              <br/>
              <strong>Andrea also possesses:</strong>
              <br/>
              <br/>
              <p>Working With Children's Check</p>
              <p>First Aid Certificate</p>
            </div>
            <br/>
            <br/>
            <center>
              <button class="btn btn-blue" data-dismiss="modal" aria-hidden="true">Close
                <i class="ion-android-close"></i>
              </button>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!--scripts loaded here for performance -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/scripts.js"></script>
  </body>

  </html>